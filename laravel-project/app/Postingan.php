<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    protected $table = 'postingan';

    protected $fillable = ['judul', 'deskripsi', 'kategori_id'];
}

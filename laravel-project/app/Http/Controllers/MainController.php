<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    // tampilkan halaman utama
    public function index () {
        return view('frontend.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postingan;
use App\Kategori;

class PostinganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // lakukan join agar menampilkan field slug pada tabel kategori dengan tabel postingan
        $postingan = Postingan::join('kategori', 'kategori.id', '=', 'postingan.kategori_id')
                    ->select('postingan.*', 'kategori.slug')
                    ->latest()->paginate(5);
        return view('admin.postingan.index', compact('postingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('admin.postingan.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'kategori' => 'required',
        ]);

        Postingan::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'kategori_id' => $request->kategori
        ]);

        return redirect()->route('postingan.index')->with('toast_success', 'Postingan Berhasil di tambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // lakukan join agar menampilkan field nama_kategori pada tabel kategori dengan tabel postingan
        $postingan = Postingan::join('kategori', 'kategori.id', '=', 'postingan.kategori_id')
                    ->select('postingan.*', 'kategori.nama_kategori')
                    ->where('postingan.id', $id)
                    ->first();
        $kategori = Kategori::all();
        return view('admin.postingan.edit', compact('postingan', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'kategori' => 'required'
        ]);

        $data_postingan = [
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'kategori_id' => $request->kategori
        ];

        Postingan::whereId($id)->update($data_postingan);

        return redirect()->route('postingan.index')->with('toast_success', 'Postingan Berhasil di edit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postingan = Postingan::destroy($id);

        return redirect()->route('postingan.index')->with('toast_success', 'Postingan Berhasil di hapus!');
    }
}

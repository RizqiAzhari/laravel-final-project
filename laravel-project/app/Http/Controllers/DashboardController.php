<?php

namespace App\Http\Controllers;

use App\Dashboard;
use App\Slider;
use App\Kategori;
use App\Postingan;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $slider = Slider::all();
        $kategori = Kategori::all();
        $postingan = Postingan::all();
        return view('admin.dashboard.index', compact('slider', 'kategori', 'postingan'));
    }
}

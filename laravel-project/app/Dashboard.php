<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    public function slider()
    {
        $this->hasMany('App\Slider');
    }

    public function kategori()
    {
        $this->hasMany('App\Kategori');
    }

    public function postingan()
    {
        $this->hasMany('App\Postingan');
    }
}

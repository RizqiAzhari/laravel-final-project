@extends('backend.konten')

@section('judul', 'postingan')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-2">
            <div class="mb-2 float-lg-left">
            <a href="{{route('postingan.create')}}" class="btn btn-primary">Tambah postingan</a>
            </div>
            {{-- Kategori yang disimpan hanya bisa disimpan 1 kali --}}
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Deskripsi</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($postingan as $item)
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$item->judul}}</td>
                            <td>{{$item->deskripsi}}</td>
                            <td><em>#{{$item->slug}}</em></td>
                            <td width="15%">
                                <form action="{{route('postingan.destroy',$item->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="{{route('postingan.edit', $item->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$postingan->links()}}
        </div>
    </div>
</div>

@endsection
@extends('backend.konten')

@section('judul', 'Edit Postingan')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('postingan.update', $postingan->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="formGroupExampleInput">Edit Judul Postingan</label>
                        <input type="text" class="form-control" value="{{$postingan->judul}}" name="judul" id="formGroupExampleInput" placeholder="Masukkan Judul Postingan">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Edit Deskripsi</label>
                        <input type="text" class="form-control" value="{{$postingan->deskripsi}}" name="deskripsi" id="formGroupExampleInput" placeholder="Masukkan Deskripsi">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Edit Kategori yang Berkaitan</label>
                        <select class="form-control select2 select2-hidden-accessible" name="kategori" data-select2-id="1" tabindex="-1" aria-hidden="true">
                            <option selected="selected" data-select2-id="{{$postingan->kategori_id}}" value="{{$postingan->kategori_id}}">{{$postingan->nama_kategori}}</option>
                            @foreach ($kategori as $item)
                                <option data-select2-id="{{$item->id}}">{{$item->nama_kategori}}</option>
                            @endforeach
                        </select>
                    </div>
                    <a href="{{route('postingan.index')}}" class="btn btn-primary btn-sm">Kembali</a>
                    <button type="submit" class="btn btn-success btn-sm">Simpan postingan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection
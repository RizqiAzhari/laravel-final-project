@extends('backend.konten')

@section('judul', 'Tambah Postingan')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('postingan.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Judul postingan</label>
                            <input type="text" class="form-control" name="judul" id="formGroupExampleInput" placeholder="Masukkan Judul Postingan">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Deskripsi</label>
                            <input type="text" class="form-control" name="deskripsi" id="formGroupExampleInput" placeholder="Masukkan Deskripsi">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Kategori yang Berkaitan</label>
                            <select class="form-control select2 select2-hidden-accessible" name="kategori" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                <option selected="selected" data-select2-id="">-</option>
                                @foreach ($kategori as $item)
                                    <option data-select2-id="{{$item->id}}" value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                @endforeach
                            </select>
                        </div>
                        <a href="{{route('postingan.index')}}" class="btn btn-primary btn-sm">Kembali</a>
                        <button type="submit" class="btn btn-success btn-sm">Simpan postingan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection
@include('backend.header')

<div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
      <div class="container">
        @yield('isikonten')
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>

  @include('backend.footer')
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('/', 'DashboardController');
Route::resource('kategori', 'KategoriController');
Route::resource('slider', 'SliderController');
Route::resource('postingan', 'PostinganController');
Route::resource('main', 'MainController');
